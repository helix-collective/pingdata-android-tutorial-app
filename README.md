# pingdata-android-tutorial-app

This repository contains a bare bones android app with the instructions to incorporate the pingdata SDK.

## Import the android aar

Instruction taken from here: https://developer.android.com/studio/projects/android-library

First import the pingdata aar library as a dependency.

## Create a handler object

 Create a PingDataUplifterHandler which will receive the final pingdata images and uplifted data.

```
PingDataUplifterHandler uplifterHandler = new PingDataUplifterHandler() {
    @Override
    public void handleUpliftedReceiptData(JSONObject receiptData) {
        // Handle uplifted receipt data here.
    }

    @Override
    public void handleReceiptImageData(List<Bitmap> images, Bitmap stitchedImage) {
        // Handle stitched receipt image here.
    }

    @Override
    public void handleErrorInUplifting(PingDataUplifterError error) {
        // Handle error in uplifting here.
    }
}
```

## Create the uplifter object.


```
PingDataUplifter uplifter = new PingDataUplifter(this, uplifterHandler, receiptJson);
```



# Pass images to uplifter and perform stitch

Some images have already been provided within the tutorial app. The following is the code to read them and add them to the uplifter.

```
BitmapFactory.Options options = new BitmapFactory.Options();
Bitmap image1 = BitmapFactory.decodeResource(getResources(), R.raw.image1, options);
Bitmap image2 = BitmapFactory.decodeResource(getResources(), R.raw.image2, options);
Bitmap image3 = BitmapFactory.decodeResource(getResources(), R.raw.image3, options);
uplifter.addReceiptImage(image1);
uplifter.addReceiptImage(image2);
uplifter.addReceiptImage(image3);
// This can be run on a non-ui thread.
uplifter.run();
```
